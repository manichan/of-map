package com.manichan.ofmap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PortalTokenModel {
    @SerializedName("token")
    @Expose
    public String token;

    @SerializedName("expires")
    @Expose
    public long expires;

    @SerializedName("ssl")
    @Expose
    public boolean ssl;
}
