package com.manichan.ofmap;


import com.esri.arcgisruntime.security.AuthenticationChallenge;
import com.esri.arcgisruntime.security.AuthenticationChallengeHandler;
import com.esri.arcgisruntime.security.AuthenticationChallengeResponse;
import com.esri.arcgisruntime.security.UserCredential;

public class CustomAuthenticationChallengeHandler implements AuthenticationChallengeHandler {
    private String token = "";

    public CustomAuthenticationChallengeHandler(String token){
        this.token = token;
    }

    @Override
    public AuthenticationChallengeResponse handleChallenge(AuthenticationChallenge authenticationChallenge) {
        if (authenticationChallenge.getType() == AuthenticationChallenge.Type.USER_CREDENTIAL_CHALLENGE) {
            return new AuthenticationChallengeResponse(
                    AuthenticationChallengeResponse.Action.CONTINUE_WITH_CREDENTIAL,
                    UserCredential.createFromToken(token,"services9.arcgis.com"));
        } else {
            return null;
        }
    }

    public void setToken(String token) {
        this.token = token;
    }
    public String getToken(){
        return token;
    }
}