package com.manichan.ofmap.ui.main;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esri.arcgisruntime.data.ServiceFeatureTable;
import com.esri.arcgisruntime.layers.FeatureLayer;
import com.esri.arcgisruntime.loadable.LoadStatus;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.view.MapView;
import com.manichan.ofmap.Constants;
import com.manichan.ofmap.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProxyFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProxyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProxyFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    MapView mMapView;
    TextView logText;
    FeatureLayer mFeatureLayer;

    private OnFragmentInteractionListener mListener;

    public ProxyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProxyFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProxyFragment newInstance(String param1, String param2) {
        ProxyFragment fragment = new ProxyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_proxy, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        mMapView = getView().findViewById(R.id.proxy_map);
        logText = getView().findViewById(R.id.proxy_log);
        setUpMap();
    }

    private void setUpMap() {
        addLog("Map init...");
        ArcGISMap map = new ArcGISMap(Basemap.Type.TOPOGRAPHIC_VECTOR, Constants.DEFAULT_MAP_CENTER_LAT, Constants.DEFAULT_MAP_CENTER_LONG, Constants.DEFAULT_MAP_LEVEL_OF_DETAIL);
        map.setMinScale(Constants.DEFAULT_MAP_MIN_SCALE);
        map.setMaxScale(Constants.DEFAULT_MAP_MAX_SCALE);
        mMapView.setMap(map);
        addLog("Map initiated.");
        addPFZDataOnline();
    }

    private void addPFZDataOnline() {
        addLog("Loading PFZ...");
        addLog("Proxy Url: http://159.65.143.72/Projects/ofish/proxy/proxy.php");
        addLog("Layer Url: https://services9.arcgis.com/D6PmsVTxfzfuh7dY/arcgis/rest/services/20200104/FeatureServer/0");
        String url = "http://159.65.143.72/Projects/ofish/proxy/proxy.php?https://services9.arcgis.com/D6PmsVTxfzfuh7dY/arcgis/rest/services/20200104/FeatureServer/0";
        ServiceFeatureTable serviceFeatureTable = new ServiceFeatureTable(url);
        serviceFeatureTable.loadAsync();
        mFeatureLayer = new FeatureLayer(serviceFeatureTable);
        mFeatureLayer.addDoneLoadingListener(() -> {
            if(mMapView!=null){
                if(mMapView.getMap()!=null){
                    if (mFeatureLayer.getLoadStatus() == LoadStatus.LOADED) {
                        ArcGISMap map = mMapView.getMap();
                        map.getOperationalLayers().add(mFeatureLayer);
                        mMapView.setMap(map);
                        addLog("PFZ loaded");
                    }
                    else{
                        addLog("Error: " + mFeatureLayer.getLoadError());
                    }
                }
            }
        });
        mFeatureLayer.loadAsync();
    }

    private void addLog(String msg){
        String currentLogs = logText.getText().toString();
        logText.setText(currentLogs + "\n" + "--->" + msg);
    }
}
