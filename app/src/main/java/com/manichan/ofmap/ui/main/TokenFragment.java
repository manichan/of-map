package com.manichan.ofmap.ui.main;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esri.arcgisruntime.data.ServiceFeatureTable;
import com.esri.arcgisruntime.layers.FeatureLayer;
import com.esri.arcgisruntime.loadable.LoadStatus;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.view.MapView;
import com.esri.arcgisruntime.security.AuthenticationManager;
import com.manichan.ofmap.Constants;
import com.manichan.ofmap.CustomAuthenticationChallengeHandler;
import com.manichan.ofmap.PortalTokenModel;
import com.manichan.ofmap.R;
import com.manichan.ofmap.TokenApi;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TokenFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TokenFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TokenFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private MapView mMapView;
    TextView logText;
    private FeatureLayer mFeatureLayer;

    private OnFragmentInteractionListener mListener;

    public TokenFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TokenFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TokenFragment newInstance(String param1, String param2) {
        TokenFragment fragment = new TokenFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_token, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        mMapView = getView().findViewById(R.id.token_map);
        logText = getView().findViewById(R.id.token_log);
        setUpMap();
    }

    private void setUpMap(){
        addLog("Map init...");
        ArcGISMap map = new ArcGISMap(Basemap.Type.TOPOGRAPHIC_VECTOR, Constants.DEFAULT_MAP_CENTER_LAT, Constants.DEFAULT_MAP_CENTER_LONG, Constants.DEFAULT_MAP_LEVEL_OF_DETAIL);
        map.setMinScale(Constants.DEFAULT_MAP_MIN_SCALE);
        map.setMaxScale(Constants.DEFAULT_MAP_MAX_SCALE);
        mMapView.setMap(map);
        addLog("Map initiated.");


        addLog("Requesting token...");
        addLog("Token Url: http://159.65.143.72/Projects/ofish/generateToken");
        Retrofit retrofitPortal = new Retrofit.Builder()
                .baseUrl("http://159.65.143.72/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        TokenApi tokenApi = retrofitPortal.create(TokenApi.class);
        HashMap<String,String> params = new HashMap<>();
        params.put("phone", "0000000000");
        tokenApi.generateToken(params).enqueue(new Callback<PortalTokenModel>() {
            @Override
            public void onResponse(Call<PortalTokenModel> call, Response<PortalTokenModel> response) {
                PortalTokenModel tokenModel = response.body();
                if(tokenModel!=null){
                    if(tokenModel.token!=null){
                        addPFZDataOnline(tokenModel.token);
                    }else{
                        addLog("Error: Token null");
                    }
                }else{
                    addLog("Error: Token null");
                }
            }

            @Override
            public void onFailure(Call<PortalTokenModel> call, Throwable t) {
                addLog("Error: Network error");
            }
        });
    }

    private void addPFZDataOnline(String token) {
        addLog("Token: " + token);
        CustomAuthenticationChallengeHandler handler = new CustomAuthenticationChallengeHandler(token);
        AuthenticationManager.setAuthenticationChallengeHandler(handler);
        String url = "https://services9.arcgis.com/D6PmsVTxfzfuh7dY/ArcGIS/rest/services/20200104/FeatureServer/0";
        ServiceFeatureTable serviceFeatureTable = new ServiceFeatureTable(url);
        serviceFeatureTable.loadAsync();
        mFeatureLayer = new FeatureLayer(serviceFeatureTable);
        mFeatureLayer.addDoneLoadingListener(() -> {
            if(mMapView!=null){
                if(mMapView.getMap()!=null){
                    if (mFeatureLayer.getLoadStatus() == LoadStatus.LOADED) {
                        ArcGISMap map = mMapView.getMap();
                        map.getOperationalLayers().add(mFeatureLayer);
                        mMapView.setMap(map);
                        addLog("PFZ loaded");
                    }
                    else{
                        addLog("Error: " + mFeatureLayer.getLoadError());
                    }
                }
            }
        });
        mFeatureLayer.loadAsync();
    }

    private void addLog(String msg){
        String currentLogs = logText.getText().toString();
        logText.setText(currentLogs + "\n"+"--->" + msg);
    }
}
