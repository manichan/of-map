package com.manichan.ofmap;

public class Constants {
    public static double DEFAULT_MAP_CENTER_LAT = 7.30;//7.5;
    public static double DEFAULT_MAP_CENTER_LONG = 80.75;//79.7;
    public static int DEFAULT_MAP_LEVEL_OF_DETAIL = 0;
    public static int DEFAULT_MAP_MIN_SCALE = 5000000;//500000
    public static int DEFAULT_MAP_MAX_SCALE = 350000;
}
